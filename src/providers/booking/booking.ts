import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { AngularFireDatabase } from '@angular/fire/database';
import { Booking, BookingStatus } from '../../model/booking/booking.model';
import * as moment from 'moment';
import { Firebase } from '@ionic-native/firebase';

@Injectable()
export class BookingProvider {
  constructor(
    private afdb: AngularFireDatabase,
    private firebase: Firebase
  ) {
    /**
     * Enable Firebase analytics data collection
     */
    firebase.setAnalyticsCollectionEnabled(true);
  }

  /**
   * Get all bookings
   */
  public getBookings() {
    return this.afdb.list('booking');
  }

  /**
   * Get all bookings by user
   * @param userId User UID
   */
  public getUserBookings(userId: string) {
    return this.afdb.list('bookings', ref => {
      return ref.orderByChild('userId').equalTo(userId);
    });
  }

  /**
   * Get all bookings by stylist
   * @param stylistId Stylist UID
   */
  public getStylistBookings(stylistId: string) {
    return this.afdb.list('bookings', ref => {
      return ref.orderByChild('stylistId').equalTo(stylistId);
    });
  }

  /**
   * Get booking by booking push key (or id)
   * @param bookingId Booking Key
   */
  public getBookingById(bookingId: string) {
    return this.afdb.list(`bookings/${bookingId}`);
  }

  /**
   * Create /booking and update /availability booked to true
   *
   * @param availId Availability Key
   * @param stylistId Stylist UID
   * @param userId User UID
   */
  public makePendingBooking(
    availId: string,
    availDate: string,
    stylistId: string,
    userId: string
  ) {
    const bookingData = {
      availabilityId: availId,
      bookedAvailSlot: availDate,
      stylistId: stylistId,
      userId: userId,
      status: BookingStatus.pending,
    };

    const bookingKey = this.afdb.database
      .ref()
      .child('/bookings')
      .push().key;

    const bookingPayload = {};
    bookingPayload[`/bookings/${bookingKey}`] = bookingData;

    console.log(bookingPayload);

    return this.afdb.database
      .ref()
      .update(bookingPayload)
      .then(() => {
        // log event to analytics
        const param ={ ...bookingData, bookingKey: bookingKey };
        console.log('Log event param: ', param);
        this.firebase.logEvent('booking_pending', param).then((result) => {
          console.log('Log event result: ', result);
        }).catch(error => {
          console.log('Log event error: ', error);
        });

        return bookingKey;
      });
  }

  public async doBookingStatusChange(bookingId: string, status: string) {
    return await this.afdb.database
      .ref(`bookings/${bookingId}`)
      .update({ status: status });
  }

  public checkBookingIsInPast(bookingDate) {
    if (bookingDate) {
      // If booking date is earlier than the current time it is a past booking
      if (bookingDate < moment().unix()) {
        return true;
      } else {
        return false;
      }
    }
  }
  public checkBookingIsInFuture(bookingDate) {
    if (bookingDate) {
      if (bookingDate > moment().unix()) {
        // booking in future
        return true;
      } else {
        return false;
      }
    }
  }
}
