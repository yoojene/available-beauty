import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import * as moment from 'moment';
import * as firebase from 'firebase';

import 'rxjs/add/operator/map';
import { of } from 'rxjs';
import { mergeMap} from 'rxjs/operators';

@Injectable()
export class MessagesProvider {
  private chatId: any;

  constructor(public afdb: AngularFireDatabase) {
    console.log('Hello MessagesProvider Provider');
  }

  // Public

  public checkIsChatThread(availId) {
    // TODO Need to account for when there is no /chat existing for user
    return this.getChatsForAvailability(availId)
      .pipe(mergeMap(res => {
        if (res.length === 0) {
          return of(false);
        }

        // only taking the first /chat
        this.chatId = res[0].key;

        return of(this.chatId);
      }));
  }

  public getChatsForUser(uid) {
    return this.afdb
      .list('chats', ref => {
        return ref.orderByChild('userId').equalTo(uid);
      })
      .snapshotChanges();
  }

  public getChatsForUserStylist(uid, stylistUid) {
    return this.afdb
      .list('chats', ref => {
        return ref.orderByChild('userId').equalTo(uid);
      })
      .snapshotChanges()
      .pipe(mergeMap(() => {
        return this.afdb
          .list('chats', ref => {
            return ref.orderByChild('stylistId').equalTo(stylistUid);
          })
          .snapshotChanges();
      }));
  }

  public getChatsForAvailability(availId) {
    console.log(availId);

    return this.afdb
      .list('chats', ref => {
        return ref.orderByChild('availabilityId').equalTo(availId);
      })
      .snapshotChanges();
  }

  public getMessagesForChat(key) {
    console.log(key);

    return this.afdb.list(`chats/${key}/messages`).valueChanges();
  }

  public addChat(userId, stylistId, availabilityId) {
    const chatData = {
      creationDate: moment().unix(),
      stylistId: stylistId,
      userId: userId,
      availabilityId: availabilityId,
    };

    const chatKey = this.afdb.database
      .ref()
      .child('chats')
      .push().key;

    const chatPayload = {};

    chatPayload[`chats/${chatKey}`] = chatData;

    return this.afdb.database
      .ref()
      .update(chatPayload)
      .then(res => console.log(res));
  }

  public addMessageForUser(chatId: any, msg: any) {
    const messageData = {
      messageDate: moment().unix(),
      messageSender: firebase.auth().currentUser.displayName,
      messageText: msg,
      senderUid: firebase.auth().currentUser.uid,
    };

    const msgKey = this.afdb.database
      .ref()
      .child(`chats/${chatId}/messages`)
      .push().key;

    const messagePayload = {};
    messagePayload[`chats/${chatId}/messages/${msgKey}`] = messageData;

    return this.afdb.database
      .ref()
      .update(messagePayload)
      .then(res => console.log(res));
  }
}
