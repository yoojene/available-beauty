import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { Storage } from '@ionic/storage';
import { from } from 'rxjs';

@Injectable()
export class StorageProvider {
  constructor(private storage: Storage) {}

  public getStorage(key) {
    return from(this.storage.get(key));
  }

  public setStorage(key, value) {
    return from(this.storage.set(key, value));
  }
}
