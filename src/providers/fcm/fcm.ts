import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { Firebase } from '@ionic-native/firebase';
import 'rxjs/add/operator/map';

@Injectable()
export class FcmProvider {
  constructor(
    public firebase: Firebase,
    public afdb: AngularFireDatabase,
    public platform: Platform
  ) {
    console.log('Hello FcmProvider Provider');
  }

  /**
   * Get the device token
   * @param uid user id
   */
  public async getToken(uid) {

    let token;

    try {
      if (this.platform.is('cordova')) {

        if (this.platform.is('android')) {
          token = await this.firebase.getToken();
        }

        if (this.platform.is('ios')) {
          token = await this.firebase.getToken();
          await this.firebase.grantPermission();
        }
      }

      return this.saveToken(token, uid);
    } catch (error) {
      console.log(`Error getToken ${error}`);
    }
  }

  /**
   * Save token under userProfile/{uid}/devices
   * @param token device token
   * @param uid current user uid
   */
  private async saveToken(token: string, uid: any) {

    console.log(`Saving token ${token} to user ${uid}`);

    if (!token) {
      return;
    }

    const ref = this.afdb.database.ref(`userProfile/${uid}/devices`);

    ref.orderByChild('token').equalTo(token).once('value', snapshot => {
      if (!snapshot.exists()) {
        const data = {
          token: token
        };

        ref.push(data);
      }

    });
  }

  /**
   * Listen to notifications and return an Observable
   */
  public listenToNotifications() {
    return this.firebase.onNotificationOpen();
  }
}
