import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { API_CONFIG, ApiConfig } from '../../model/api.config';
import { of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { User } from '../../model/users/user.model';
import firebase from 'firebase';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { UtilsProvider } from '../utils/utils';
import { Review } from '../../model/review/review.model';

@Injectable()
export class UserProvider {
  constructor(
    public http: HttpClient,
    public afdb: AngularFireDatabase,
    private utils: UtilsProvider,
    @Inject(API_CONFIG) public config: ApiConfig
  ) {}

  /**
   * Return users
   *
   */
  public getStylistUsers(): AngularFireList<User> {
    return this.afdb.list<User>('userProfile', ref => {
      return ref.orderByChild('isStylist').equalTo(true);
    });
  }

  /**
   * Check if user is stylist
   * @param uid firebase User.uid
   */
  public checkIsStylist2(uid: any) {
    return this.afdb.object<boolean>(`userProfile/${uid}/isStylist`);
  }
  /**
   * Look up userId in /userProfile and check isStylist attribute.
   * If true return profile else false
   *
   */
  public checkIsStylist(uid: any) {
    return this.afdb
      .list('userProfile', ref => {
        return ref.orderByChild('isStylist').equalTo(true);
      })
      .snapshotChanges()
      .pipe(
        mergeMap(res => {
          const keyed = this.utils.generateFirebaseKeyedValues(res);
          const user = keyed.filter(el => {
            return el.key === uid;
          });
          if (user.length > 0) {
            return of(user) as any;
          } else {
            return of(false) as any;
          }
        })
      );
  }

  public getUserById(id) {
    return this.afdb.object<User>(`userProfile/${id}`);
  }
  public getUserListById(id) {
    return this.afdb.list<User>(`userProfile/${id}`);
    // return this.afdb.list<User>(`userProfile`, ref => {
    //   return ref.key;
    // });
  }

  /**
   * update userProfile in RTDB
   *
   * @param userId uid
   * @param userObj user Object
   * @param isStylist boolean to denote stylist or user
   */
  public updateUserProfile(userId: any, userObj: any, isStylist = true) {
    console.log('updateUserProfile to promise');

    console.log(userId);
    console.log(userObj);
    const updatedUser = {
      name: userObj.name,
      emailAddress: userObj.emailAddress,
      isStylist: isStylist,
      phoneNumber: userObj.phoneNumber,
      bannerImage: null, // need to update
      // Every below is stylist only?
      bio: userObj.bio,
      stylistName: userObj.stylistName,
      stylistSkills: userObj.stylistSkills,
      mobile: userObj.mobile,
      mobileRange: userObj.mobileRange,
      baseLocation: userObj.baseLocation,
      addressLine1: userObj.addressLine1,
      addressTownCity: userObj.addressTownCity,
      addressLine2: userObj.addressLine2,
      addressCounty: userObj.addressCounty,
      addressPostcode: userObj.addressPostcode,
      galleryImages: null, // need to update
    };

    if (userObj.loadImages) {
      // TODO need to make sure this works for EditUserProfile.
      // Current flag is for StylistRegister
      updatedUser.galleryImages = userObj.galleryImages;
      updatedUser.bannerImage = userObj.bannerImage;
    }

    return this.getUserById(userId)
      .valueChanges()
      .take(1)
      .toPromise()
      .then((userres: any) => {
        const existUser = userres;

        const combined = { ...existUser, ...updatedUser };

        console.log(combined);

        const userPayload = {};

        userPayload[`userProfile/${userId}`] = combined;

        return this.afdb.database
          .ref()
          .update(userPayload)
          .then(res => console.log(res));
      });
  }

  // Skills

  public setStylistSkills(uid, skills) {
    this.afdb.database
      .ref()
      .child(`userProfile/${uid}/skills`)
      .set(skills);
  }

  public setStylistAvailableSlots(uid, slots) {
    this.afdb.database
      .ref('userProfile')
      .child(`${uid}/availabilitySlots`)
      .set(slots);
  }

  // Reviews

  public async addReview(receiverUid: any, review: any, starRating: any) {
    // TODO Do we want to associate the Review with the availability slot as
    // well as the stylist?
    const reviewData = {
      senderUid: firebase.auth().currentUser.uid, // User
      receiverUid: receiverUid, // Mostly the Stylist but could be the user too
      reviewText: review,
      starRating: starRating,
    };

    const reviewKey = this.afdb.database
      .ref()
      .child('reviews/')
      .push().key;

    const reviewPayload = {};
    reviewPayload[`/reviews/${reviewKey}`] = reviewData;

    console.log(reviewPayload);

    const result = await this.afdb.database.ref().update(reviewPayload);

    console.log(result);

    return result;
  }

  public getReviewByReceiver(userId: any) {
    return this.afdb.list<Review>('reviews', ref => {
      return ref.orderByChild('receiverUid').equalTo(userId);
    });
  }

  /**
   * Brute way to check if primary information for stylists is complete.
   * @param user User
   */
  public isComplete(user: User) {
    if (user.stylistName !== null) {
      if (user.baseLocation !== null) {
        if (user.addressLine1 !== null) {
          if (user.addressLine2 !== null) {
            if (user.addressPostcode !== null) {
              if (user.addressCounty !== null) return true;
            }
          }
        }
      }
    }

    return false;
  }
}
