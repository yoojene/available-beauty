import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Platform } from 'ionic-angular';
import { StorageProvider } from '../storage/storage';

@Injectable()
export class ConfigurationProvider {
  constructor(
    public http: Http,
    private plt: Platform,
    private storage: StorageProvider
  ) {
    console.log('Hello ConfigurationProvider Provider');
  }

  public loadConfiguration() {
    console.log('Loading Configurations');
    const firebaseRemoteConfigDefaults = {
      default_available_slots: 5
    };
    const timeout = 600;
    this.plt
      .ready()
      .then(() => {
        if (this.plt.is('cordova')) {
          (<any>window).FirebasePlugin
          .setDefaults(firebaseRemoteConfigDefaults, _ => {
            (<any>window).FirebasePlugin.fetch(timeout, (fetchResult) => {
              console.log(JSON.stringify(fetchResult));
              (<any>window).FirebasePlugin
              .activateFetched((activateFetchedResult) => {
                console.log(JSON.stringify(activateFetchedResult));
                
                // load landing text examples
                (<any>window).FirebasePlugin
                .getValue('default_available_slots', (value) => {
                  console.log(`Set storage ${value}`);
                  this.storage
                    .setStorage('default_available_slots', value);
                }, reason => {
                  console.error(`Error ${reason}`);
                });
              });
            });
          });
        }
      });
  }
}
