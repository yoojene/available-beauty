import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  App,
} from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  public settingsTitle = 'Settings';
  public notificationsLabel = 'Notifications';
  public termsLabel = 'Terms of Service';
  public logOutLabel = 'Log Out';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private afauth: AngularFireAuth,
    private viewCtrl: ViewController,
    private app: App
  ) {}

  public ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  public doLogOut() {
    console.log(`Loging out ${this.afauth.auth.currentUser.displayName}`);
    this.afauth.auth.signOut().then(() => {
      this.viewCtrl.dismiss();
      console.info('Logout success... redirecting to Landing Page');
      // this.navCtrl.setRoot('LandingPage');
      this.app.getRootNav().setRoot('LandingPage');
    });
  }
}
