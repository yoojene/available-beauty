import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';
import * as firebase from 'firebase';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {
  public backgrounds = [
    'assets/img/background/background-1.jpg',
    'assets/img/background/man-studio-portrait-light-90764.jpeg',
    'assets/img/background/pexels-photo-556665_1.jpg',
    'assets/img/background/pexels-photo-97218_1.jpg',
  ];

  public tourSlides: any = [
    {
      slideText: 'Find salons and beauty specialists available near to you',
    },
    {
      slideText: 'View recommendations, ratings and  reviews ',
    },
    {
      slideText: 'Get deals on the treatments you want',
    },
  ];

  public lookingTitle = 'Looking for Beauty';
  public offeringTitle = 'Offering Beauty';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private splash: SplashScreen,
    private af: AngularFireAuth,
    private user: UserProvider
  ) {
    this.af.auth.onAuthStateChanged(firebaseUser => {
      if (firebaseUser && !firebaseUser.isAnonymous) {
        const _user = this.user
          .getUserById(firebaseUser.uid)
          .valueChanges()
          .subscribe(userData => {
            if (!userData.hasOwnProperty('stylistProfileComplete')) {
              this.navCtrl.setRoot('TabsPage', {
                isStylist: userData.isStylist,
              });
              _user.unsubscribe();
            }
          });
      }
    });
  }

  public ionViewWillEnter() {
    this.splash.hide();
  }

  // Lifecycle
  public ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
  }

  // Public
  public async openPage(el) {
    console.log(el);
    const type = el._elementRef.nativeElement.name;

    if (type === 'Looking') {
      await firebase.auth().signInAnonymously();
      // this.navCtrl.setRoot('LandingPage');
      this.navCtrl.push('TabsPage', { isStylist: false });
    } else {
      this.navCtrl.push('LoginPage', { isStylist: true });
    }
  }
}
