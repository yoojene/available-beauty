import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  Platform
} from 'ionic-angular';
import * as moment from 'moment';
import { AvailabilityProvider } from '../../providers/availability/availability';
import * as firebase from 'firebase';
import { UtilsProvider } from '../../providers/utils/utils';
import { Moment } from 'moment';
import { UserProvider } from '../../providers/user/user';
import { StorageProvider } from '../../providers/storage/storage';
import { BuyMoreSlotsPage } from '../buy-more-slots/buy-more-slots';
import { Dialogs } from '@ionic-native/dialogs';

@IonicPage()
@Component({
  selector: 'page-availability',
  templateUrl: 'availability.html',
})
export class AvailabilityPage {
  public availabilitySubHeader = 'Mark the times you are available';
  public availabilitySlotLabel = 'Remaining Available Slots : ';

  public morningText = 'morning';
  public afternoonText = 'afternoon';
  public eveningText = 'evening';
  public showNext: boolean;
  public dayOfWeekFmt: string = 'ddd Do MMM';
  public availTimeFmt: string = 'HH:mm';
  public today: any;
  public morningStart: any = moment()
    .hour(8)
    .minutes(0)
    .seconds(0);
  public morningEndTime: string = '12:00';
  public afternoonEndTime: string = '16:00';

  public availableAMDates: any;
  public availablePMDates: any;
  public availableEveDates: any;

  public stylistId: string;

  public schedule: any;

  public entryLoader: any;

  private defaultAvailableSlots: any;

  public currentTimestampInEpoch: number;

  public numberOfAvailableSlots: number;

  public selectedSlots: any = [];
  public numberOfSelectedSlots: number = 0;

  private confirmMessage: string = 'You will not be able to undo this action.';
  private confirmTitle = 'Continue';

  private removeSlotMessage:
    string = 'Removing this slot will no longer your' +
    ' increase number of available slots.';
  private removeSlotTitle: string = 'Confirm Remove';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public avail: AvailabilityProvider,
    public user: UserProvider,
    private _utils: UtilsProvider,
    private _loading: LoadingController,
    private storage: StorageProvider,
    private modalCtrl: ModalController,
    private dialogs: Dialogs,
    private platform: Platform
  ) {
    this.storage
    .getStorage('default_available_slots')
    .subscribe(result => (this.defaultAvailableSlots = result));
  }

  // Lifecycle
  public ionViewWillEnter() {
    console.log('ionViewWillEnter');
    this.stylistId = firebase.auth().currentUser.uid;
    this.entryLoader = this._loading.create();
    this.entryLoader.present().then(() => {
      this.generateAvailabilitySchedule();
      this.getAvailabilitySlots();
      // });
    });
    this.doCheckShowNext();
  }

  public ionViewWillLeave() {
    this.numberOfSelectedSlots = 0;
    this.selectedSlots = [];   
  }


  // Private

  /**
   * Set up length of schedule on availability page and slot length/duration
   * Default is 1 day for 7 days ahead and 18 slots of 30m per day
   * Will not show slot that is in the past of current time
   */

  private generateAvailabilitySchedule() {
    // Generate the schedule
    this.schedule = this.generateSchedule(
      moment(),
      this.dayOfWeekFmt,
      1,
      'day',
      7
    );

    console.log(this.schedule);
    // Generate the slots per schedule
    for (let y = 0; y < this.schedule.length; y++) {
      const morningSlots = [];
      morningSlots.push(
        this.avail.generateAvailabilitySlots(
          moment(this.schedule[y].date, this.dayOfWeekFmt)
            .hour(8)
            .minutes(0)
            .seconds(0),
          this.availTimeFmt,
          30,
          'm',
          8,
          'morning'
        )
      );

      const afternoonSlots = [];
      afternoonSlots.push(
        this.avail.generateAvailabilitySlots(
          moment(this.schedule[y].date, this.dayOfWeekFmt)
            .hour(12)
            .minutes(0)
            .seconds(0),
          this.availTimeFmt,
          30,
          'm',
          8,
          'afternoon'
        )
      );
      const eveningSlots = [];
      eveningSlots.push(
        this.avail.generateAvailabilitySlots(
          moment(this.schedule[y].date, this.dayOfWeekFmt)
            .hour(16)
            .minutes(0)
            .seconds(0),
          this.availTimeFmt,
          30,
          'm',
          8,
          'evening'
        )
      );
      // Collapse into single array
      const morningMerged = [].concat.apply([], morningSlots);
      this.schedule[y].morningSlots = morningMerged;
      const afternoonMerged = [].concat.apply([], afternoonSlots);
      this.schedule[y].afternoonSlots = afternoonMerged;
      const eveningMerged = [].concat.apply([], eveningSlots);
      this.schedule[y].eveningSlots = eveningMerged;
    }

    console.log(this.schedule);

    // This is a bit naff, ideally want to seed schedule from what's already
    // taken, and not the otherway around
    const stylistId = this.stylistId;
    console.log(stylistId);

    this.avail
      .getStylistAvailability(stylistId)
      .snapshotChanges()
      .subscribe(res => {
        const results = this._utils.generateFirebaseKeyedValues(res);
        console.log('Results: ', results);
        results.forEach(resres => {
          this.schedule.forEach(sched => {
            sched.morningSlots.forEach(el => {
              if (el.epoch === resres.datetime) {
                el.disabled = true;
                el.key = resres.key;
              }
            });
            sched.afternoonSlots.forEach(el => {
              if (el.epoch === resres.datetime) {
                el.disabled = true;
                el.key = resres.key;
              }
            });
            sched.eveningSlots.forEach(el => {
              if (el.epoch === resres.datetime) {
                el.disabled = true;
                el.key = resres.key;
              }
            });
          });
        });
      });

    this.entryLoader.dismiss();
  }

  // hide slot that is already in the past
  public hideSlot(date){
    if(!this.currentTimestampInEpoch){
      this.currentTimestampInEpoch = moment().valueOf() / 1000;
    }

    if(date.epoch < this.currentTimestampInEpoch){
      return true;
    }
  }

  // hide the select all timeslot button
  public hideTimeSlot(timeslot , index){

    // this filter only applies on first index
    if(index !== 0){
      return false;
    }

    const currentTime = moment();
    if(timeslot === 'morning'){
      const morningEnd = moment(this.morningEndTime , this.availTimeFmt);

      return morningEnd.isBefore(currentTime);
    }

    if(timeslot === 'afternoon'){
      const afternoonEnd = moment(this.afternoonEndTime , this.availTimeFmt);

      return afternoonEnd.isBefore(currentTime);
    }

  }

  public goToHome() {
    console.log('go to home');
    console.log(this.navCtrl.length());
    if (this.navCtrl.length() === 1) {
      this.navCtrl.push('TabsPage', {
        isStylist: true,
      });
    }
  }

  /**
   * Create the parent schedule object
   *
   * @param startDate - Date to start schedule
   * @param dateFmt - moment date/time format
   * @param interval 1
   * @param dateUnit - moment date format
   * @param runsFor - how long the schedule runs for
   */
  private generateSchedule(
    startDate: Moment,
    dateFmt: string,
    interval: number,
    dateUnit: moment.unitOfTime.DurationConstructor,
    runsFor: number
  ) {
    const schedule = [{ date: startDate.format(dateFmt), unit: dateUnit }];
    let loopInt = interval;
    for (let x = 0; x < runsFor; x++) {
      schedule.push({
        date: moment(startDate)
          .add(loopInt, dateUnit)
          .format(dateFmt),
        unit: dateUnit,
      });
      loopInt = loopInt + interval;
    }
    console.log('schedule');
    console.log(schedule);

    return schedule;
  }

  // Public
  /**
   * Mark the given slot taken
   *
   */
  public toggleSlot(option, optionobj) {
    optionobj.forEach(el => {
      if (option.time === el.time && option.date === el.date) {
        if (!option.disabled) {
          // Don't create slot if available slot is 0
          if(this.numberOfAvailableSlots <= 0){
            return;
          }
          el.disabled = !option.disabled;
          this.selectedSlots.push(el);
          this.numberOfSelectedSlots++;
        } else {

          if (this.isInSelectedSlots(el)) {
            this.removeSelectedSlot(el);
            this.numberOfSelectedSlots--;
            option.disabled = !option.disabled;
            el.disabled = option.disabled;
          } else {
            // this.removeConfirmedSlot(el, option);
            // option.disabled = !option.disabled;
            // el.disabled = option.disabled;
          }
        }
      }
    });
  }

  /**
   * Used to differentiate newly selected slots from confirmed slots
   * @param slot Available Slot Object
   */
  private isInSelectedSlots(slot) {
    let inSelectedSlots = false;
    this.selectedSlots.forEach(selectedSlot => {
      if (slot.epoch === selectedSlot.epoch) {
        inSelectedSlots = true;

        return;
      }
    });

    return inSelectedSlots;
  }

  /**
   * Remove the selected slot if slot button is untoggled
   * @param slot Availability Slot Object
   */
  private removeSelectedSlot(slot) {
    this.selectedSlots.forEach((selectedSlot, index) => {
      if (selectedSlot.epoch === slot.epoch) {
        this.selectedSlots.splice(index, 1);
      }
    });
  }

  /**
   * Removes a confirmed availability slot
   * @param slot Confirmed Availability Slot
   */
  private removeConfirmedSlot(slot, option) {
    this.platform.ready().then(() => {
      this.dialogs.confirm(
          this.removeSlotMessage,
          this.removeSlotTitle,
          [ 'Remove', 'Cancel' ]
        )
        .then(btnIndex => {
          if (btnIndex === 1) {
            this.avail
              .removeSelectedSlot(slot.key)
              .then(() => {
                option.disabled = !option.disabled;
                slot.disabled = option.disabled;
              })
              .catch((err) => {
                console.error('Error: ', err.message);
              });
          }
        });
    });
  }

  // /**
  //  * Mark all slots on the day taken
  //  *
  //  * @param {any} slot
  //  * @memberof AvailabilityPage
  //  */
  // setAllSlotsTaken(slot) {
  //   slot.slots.forEach(el => {
  //     el.disabled = !el.disabled;
  //   });
  // }

  /**
   * Mark all slots for a sub group on the day taken
   *
   */
  public setAllSlotsTaken(slot, subgroup) {
    if (subgroup === 'morning' || subgroup === 'all') {
      slot.morningSlots.forEach(el => {
        el.disabled = !el.disabled;
      });
    }
    if (subgroup === 'afternoon' || subgroup === 'all') {
      slot.afternoonSlots.forEach(el => {
        el.disabled = !el.disabled;
      });
    }
    if (subgroup === 'evening' || subgroup === 'all') {
      slot.eveningSlots.forEach(el => {
        el.disabled = !el.disabled;
      });
    }
  }

  private updateTakenSlots(bookedSlots) {
    console.log(bookedSlots);
    console.log(this.schedule);

    bookedSlots.forEach(el => {
      // return momel.datetime;
    });
  }

  private doCheckShowNext() {
    if (this.navCtrl.length() === 2) {
      console.log('showing');
      this.showNext = true;
    } else {
      console.log('not showing');
      this.showNext = false;
    }
  }

  private doShowPeriod(period) {
    // For filtering am/pm/eve if required
    console.log(period);

    console.log(period._elementRef.nativeElement.name);
    const filtered = this.availableAMDates.filter(el => {
      return el.period === period._elementRef.nativeElement.name;
    });

    console.log(filtered);
    // this.availableAMDates = filtered;
  }

  // Subscribe to availability slots

  private getAvailabilitySlots(){
    this.avail
    .getNumberOfAvailabilitySlots(this.stylistId)
    .snapshotChanges()
    .subscribe(action => {
      console.log(`User number of availability slots ${action.payload.val()}`);
      this.numberOfAvailableSlots = action.payload.val();
      this.storage.setStorage(
        'current_available_slots',
        this.numberOfAvailableSlots
      );
    }, error => {
      console.log('Error: ', error.message);
    });
  }

  public doOpenBuyMoreSlotsModal() {
    const modal = this.modalCtrl.create(BuyMoreSlotsPage);
    modal.present();
  }

  public confirmSelectedSlots() {
    let message;
    let confirmMessage;
    message =`You selected ${this.numberOfSelectedSlots} slots to advertise.`;
    confirmMessage = `${message} ${this.confirmMessage}`;
    this.dialogs.confirm(
        confirmMessage,
        this.confirmTitle,
        [ 'Submit' , 'Cancel']
      )
      .then(btnIndex => {
        console.log(`Button index clicked ${btnIndex}`);
        if (btnIndex === 1) {
          const loader = this._loading.create();
          loader.present().then(() => {
            this.selectedSlots.forEach((slot, index) => {
              this.avail
                .setAvailabilityTaken(slot.epoch, this.stylistId)
                .then(() => {
                  this.avail
                    .decreaseNumberOfSlot(this.stylistId, 1);

                  if (index === this.selectedSlots.length - 1) {
                    this.selectedSlots = [];
                    this.numberOfSelectedSlots = 0;
                    loader.dismiss();
                  }
                });
            });
          });
        }
      });
  }
}
