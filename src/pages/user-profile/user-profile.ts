import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
} from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';

import firebase from 'firebase';
import { SettingsPage } from '../settings/settings';
import { StorageProvider } from '../../providers/storage/storage';

import { EditUserProfilePage } from '../edit-user-profile/edit-user-profile';
import { BuyMoreSlotsPage } from '../buy-more-slots/buy-more-slots';
import { AngularFireDatabase } from '@angular/fire/database';

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {
  public editProfileText = 'Edit Profile';
  public name: string;
  public email: string;

  public user: any;
  public isStylist: boolean;
  public stylist$: any;
  public style: any;

  public stylistSkills: Array<string>;

  public hideOnModal = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private afauth: AngularFireAuth,
    private afdb: AngularFireDatabase,
    private modalCtrl: ModalController,
    private storage: StorageProvider
  ) {}

  // Lifecycle
  protected ionViewWillEnter() {
    console.log('ionViewWillEnter UserProfilePage');
    const user = this.navParams.get('user');

    console.log(user);

    this.hideOnModal = true;
    if (user) {
      this.hideOnModal = false;
    }

    this.getUserProfile(user);
    console.log('hideOnModal  ', this.hideOnModal);
  }

  // Public

  public doEditProfile() {
    console.log('editing PRofile');
    const editProfileModal = this.modalCtrl.create(EditUserProfilePage);

    editProfileModal.onDidDismiss(data => {
      console.log('dismissed ', data);
      this.getUserProfile();
    });

    editProfileModal.present();
  }

  public doOpenSettings() {
    const settingsModal = this.modalCtrl.create(SettingsPage);

    settingsModal.onDidDismiss(data => {
      console.log('dismissed ', data);
    });

    settingsModal.present();
  }

  public doLogOut() {
    console.log('sign out valled');
    this.afauth.auth.signOut().then(() => {
      this.navCtrl.setRoot('LandingPage');
    });
  }

  public doOpenBuyMoreSlots() {
    const buyMoreSlotsModal = this.modalCtrl.create(BuyMoreSlotsPage);
    buyMoreSlotsModal.present();
  }

  // Private

  private getUserProfile(user?) {
    // TODO Make this dynamic with userId passed in from navParams
    console.log(this.afauth.auth.currentUser.uid);
    console.log(user);
    let userId;
    if (user) {
      userId = user;
    } else {
      userId = this.afauth.auth.currentUser.uid;
    }

    this.afdb.database
      .ref('/userProfile')
      .child(userId)
      .once('value')
      .then(res => {
        console.log(res.val());

        this.user = res.val();
        this.isStylist = this.user.isStylist;
        this.stylistSkills = this.user.skills;
        console.log('skills', this.stylistSkills);
        console.log(`isStylist? ${this.isStylist}`);
      });
  }
}
