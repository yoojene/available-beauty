import { Component, ViewChild } from '@angular/core';
import { HomePage } from '../home/home';
import { BookingsPage } from '../bookings/bookings';

import { UserProfilePage } from '../user-profile/user-profile';

import {
  NavParams,
  NavController,
  Platform,
  IonicPage,
  Events,
  Tabs,
  ToastController,
} from 'ionic-angular';

import { AvailabilityPage } from '../availability/availability';
import { FcmProvider } from '../../providers/fcm/fcm';
import { tap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserProvider } from '../../providers/user/user';
import { SplashScreen } from '@ionic-native/splash-screen';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  @ViewChild(Tabs)
  public tabs: Tabs;

  public showTabs: boolean;
  public showkeyboard: boolean;
  public hasRealProfile: boolean = false;
  public isStylist: boolean;
  public anonymousUser: boolean;

  public tab1Root: any;
  public tab1Title: any;
  public tabTitle: any;
  public tabIcon: any;
  public tab1Icon: any;

  public tab3Root: any = BookingsPage;
  public tab4Root: any = UserProfilePage;
  public tab1Params = { id: 0 };
  public tab3Params = { id: 2 };
  public tab4Params = { id: 3 };

  constructor(
    private fcm: FcmProvider,
    private af: AngularFireAuth,
    private navParams: NavParams,
    private splash: SplashScreen,
    private toastCtrl: ToastController,
    private platform: Platform
  ) {
    this.splash.hide();
    const currentUser = this.af.auth.currentUser;
    this.anonymousUser = currentUser.isAnonymous;

    console.log(`Welcome TabPage User ${JSON.stringify(currentUser)}`);
    
    this.platform.ready().then(() => {
      this.fcm.getToken(currentUser.uid);

      this.fcm.listenToNotifications().pipe(
        tap(msg => {
          
          console.log(`Message payload ${JSON.stringify(msg)}`);
          console.log(`Messaged tapped? ${msg.tap}`);
          
          if (msg.tap) {
            this.tabs.select(1);
          }

          const toast = this.toastCtrl.create({
            message: msg.body,
            duration: 3000,
            position: 'top',
            dismissOnPageChange: true,
            showCloseButton: true,
            closeButtonText: 'View'
          });
          toast.present();

          toast.onDidDismiss((data, role) => {
            if (role == 'close') {
              this.tabs.select(1);
            }
          });
        })
      )
      .subscribe()
    });

    this.isStylist = this.navParams.get('isStylist');
    if (this.isStylist) {
      this.tab1Root = AvailabilityPage;
      this.tab1Title = 'Availability';
      this.tabTitle = this.tab1Title;
      this.tab1Icon = 'calendar';
      this.tabIcon = this.tab1Icon;
    } else {
      this.tab1Root = HomePage;
      this.tab1Title = 'Search';
      this.tabTitle = this.tab1Title;
      this.tab1Icon = 'search';
      this.tabIcon = this.tab1Icon;
    }
    // events.subscribe('change-stylist-profile-tab', (tab, id, param) => {
    //   this.tab2Params.id = id;
    //   this.tab2Params.user = param;
    //   this.tabs.select(tab);
    // });
  }

  public ionViewWillEnter() {
    // this.keyboard.onKeyboardShow().subscribe(() => {
    //   this.showkeyboard = true;
    // });
    // this.keyboard.onKeyboardHide().subscribe(() => {
    //   this.showkeyboard = false;
    // });
    // Dynamically set the .mdi class to show the MD icon for Consultas
    // let parentnode = document.getElementById('tab-t0-1');
    // let childnode = parentnode.getElementsByClassName('tab-button-icon')[0];
    // childnode.className += ' mdi';
  }
}
