import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { PasswordValidation } from '../../forms/password-validation';

import { UserProvider } from '../../providers/user/user';
import { LocationProvider } from '../../providers/location/location';

import { AuthProvider } from '../../providers/auth/auth';
import { StorageProvider } from '../../providers/storage/storage';
import { AngularFireDatabase } from '@angular/fire/database';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  public registerForm: any;
  public regError: any;
  public invalidReg = false;
  private coords: any;

  public passwordError = 'Passwords do not match';
  public minLengthError = 'Passwords must be 6 characters or more';
  public termsConditionsText: any =
    '<p> By signing up you agree to the <a href="">Terms and Conditions</a> and <a href=""> Privacy Policy</a></p>';

  private isStylist: boolean;

  private defaultAvailableSlots: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public user: UserProvider,
    public location: LocationProvider,
    private auth: AuthProvider,
    private storage: StorageProvider,
    private afdb: AngularFireDatabase
  ) {
    this.registerForm = formBuilder.group(
      {
        displayName: ['', Validators.required],
        emailAddress: [
          '',
          Validators.compose([Validators.required, Validators.email]),
        ],
        password: [
          '',
          Validators.compose([Validators.minLength(6), Validators.required]),
        ],
        confpassword: [
          '',
          Validators.compose([Validators.minLength(6), Validators.required]),
        ],
      },
      { validator: PasswordValidation.MatchPassword }
    );
    this.getDefaultAvailableSlots();
  }

  private getDefaultAvailableSlots() {
    this.afdb.database.ref('/settings').on('value', snapshot => {
      let settingsSnapshot = snapshot.val();
      this.defaultAvailableSlots = settingsSnapshot.defaultAvailableSlots;
      console.log(`Default available slots ${this.defaultAvailableSlots}`);
    });
  }

  // Lifecycle

  public ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    this.isStylist = this.navParams.get('isStylist');

    this.storage
      .getStorage('geolocation')
      .subscribe(res => (this.coords = res));

    // this.storage
    //   .getStorage('default_available_slots')
    //   .subscribe(
    //     res => {
    //       this.defaultAvailableSlots = res;
    //       console.log(`Default Available Slots ${this.defaultAvailableSlots}`);
    //     },
    //     err => {
    //       console.warn(`Error ${err}`);
    //     }
    //   );
  }

  // Public
  public doRegister() {
    console.log(this.registerForm.value);

    if (this.registerForm.valid) {
      const user = this.registerForm.value;
      user.homeLocation = this.coords;
      user.isStylist = this.navParams.get('isStylist');
      user.availabilitySlots = this.defaultAvailableSlots;
      this.auth
        .doRegister(user, this.isStylist)
        .then(res => {
          console.log('User Registered : ' + JSON.stringify(res));
          if (user.isStylist) {
            this.navCtrl.push('StylistRegisterPage');
          } else {
            this.navCtrl.push('TabsPage', { isStylist: user.isStylist });
          }
        })
        .catch(err => {
          this.invalidReg = true;
          this.regError = err.message;
          console.error(err);
        });
    } else {
      // Error!
      this.navCtrl.pop();
      // TODO: Show visual error message
    }
  }
  /**
   * Toggle password or text input
   *
   * @param input
   */
  public showPassword(input) {
    input.type = input.type === 'password' ? 'text' : 'password';
  }
}
