import { Component, ViewChild } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ActionSheetController,
  Platform,
  Slides,
} from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { StorageProvider } from '../../providers/storage/storage';
import { Storage } from '@ionic/storage';

import { PhoneNumberValidator } from '../../validators/phone-number.validators';

import {
  NativeGeocoderReverseResult,
  NativeGeocoderForwardResult,
} from '@ionic-native/native-geocoder';
import { Camera } from '@ionic-native/camera';

import { LocationProvider } from '../../providers/location/location';
import { PhotoProvider } from '../../providers/photo/photo';
import * as firebase from 'firebase';
import {
  AngularFireDatabase,
  AngularFireAction,
  DatabaseSnapshot,
} from '@angular/fire/database';
import 'rxjs/add/operator/takeLast';
import { UserProvider } from '../../providers/user/user';
import { ImagePicker } from '../../../node_modules/@ionic-native/image-picker';
import { SkillsProvider } from '../../providers/skills/skills';
import { Observable } from 'rxjs/Observable';
import { FirebaseStoragePaths } from '../../config/firebase.config';

@IonicPage()
@Component({
  selector: 'page-stylist-register',
  templateUrl: 'stylist-register.html',
})
export class StylistRegisterPage {
  @ViewChild(Slides) public slides: Slides;
  public pageSubheader = 'OK, now enter some details to set up your profile..';
  public stylistNameLabel = 'Stylist Name';
  public stylistHeaderLabel = 'Enter a Salon or Stylist name';
  public bioLabel =
    'A short description of your business and what services you offer';
  public bioPlaceholder = 'Write a few details here';
  public servicesLabel = 'Select the services you provide';
  public phoneNumberLabel = 'Telephone Number';
  public locationLabel = 'Where are you located?';
  public useMyCurrentLocationLabel = 'Use My Current';
  public enterLocationLabel = 'Enter your location';
  public findAddressLabel = 'Find Address';

  public addressLine1Label = 'Address Line 1';
  public addressLine2Label = 'Address Line 2';
  public addressTownCityLabel = 'Town / City';
  public addressCountyLabel = 'County';
  public addressPostcodeLabel = 'Post Code';

  public mobileLabel = 'Are you a mobile stylist?';
  public mobileRangeLabel = 'How far will you travel from your base?';
  public mobileRangePlaceholder = 'Enter in miles';
  public profilePhotoLabel = 'Add an avatar to your profile';
  public profilePhotoText =
    'This can be a photo of you or a logo for your business';
  public galleryImagesLabel = 'Upload gallery images now?';
  public galleryImagesText =
    'You can add a few images so that customers can see what services you offer.  These will be displayed on your listing.';
  public orLabel = 'Or';
  public nextButtonText = 'Next';
  public yesButtonText = 'Yes';
  public noButtonText = 'No';
  public choosePhotoButtonText = 'Choose Photo';
  public useCameraButtonText = 'Take Photo';
  public finishButtonText = 'Finish';
  public stylistRegForm: any;
  public showMobileRange: boolean;
  public showAddressForm: boolean;
  private coords: any;
  public loadProgress: any = 0;
  public downloadUrls = [];
  public stylistKey: any;

  public activeSlideIdx: any = 0;

  public outlineYes = true;
  public outlineNo = true;

  public showPhotoSpinner = false;

  // tslint:disable-next-line:array-type
  public skillslist$: Observable<AngularFireAction<DatabaseSnapshot<{}>>[]>;
  public skill: any;

  private stylistSkills: any;
  public selectedSkills: any;

  public profileUrl = 'assets/img/no-avatar.png';

  constructor(
    public navCtrl: NavController,
    private plt: Platform,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public storage: StorageProvider,
    public ionicstorage: Storage,
    public location: LocationProvider,
    public camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public photo: PhotoProvider,
    public afdb: AngularFireDatabase,
    public user: UserProvider,
    public skillsProvider: SkillsProvider,
    private imagePicker: ImagePicker
  ) {
    this.stylistRegForm = formBuilder.group({
      phoneNumber: [
        '',
        [Validators.required, PhoneNumberValidator.isPhoneNumber],
      ],
      stylistName: ['', Validators.required],
      isMyLocation: [true, Validators.required],
      locationLookup: [''],
      baseLocation: ['', Validators.required],
      addressLine1: ['', Validators.required],
      addressLine2: ['', Validators.required],
      addressTownCity: ['', Validators.required],
      addressCounty: ['', Validators.required],
      addressPostcode: ['', Validators.required],
      bio: ['', Validators.required],
      stylistSkills: ['', Validators.required],
      mobile: [false, Validators.required],
      mobileRange: [''],
      loadImages: [false, Validators.required],
    });
  }

  // Lifecycle

  public ionViewDidLoad() {
    console.log('ionViewDidLoad StylistRegisterPage');

    this.skillslist$ = this.skillsProvider.getSkillGroups().snapshotChanges();

    this.slides.lockSwipeToNext(true);
    this.slides.lockSwipeToPrev(true);
    this.showAddressForm = false;
    this.stylistRegForm.get('mobile').valueChanges.subscribe(val => {
      this.showMobileRange = val;
      if (val) {
        this.stylistRegForm
          .get('mobileRange')
          .setValidators([Validators.required]);

        this.stylistRegForm.get('mobileRange').updateValueAndValidity();
      }
    });

    this.stylistRegForm.controls['stylistSkills'].valueChanges.subscribe(
      val => {
        this.stylistSkills = val;
      }
    );

    this.stylistRegForm.get('loadImages').valueChanges.subscribe(val => {
      if (val) {
        console.log(val);
        this.showImageActionSheet();
      }
    });
  }

  // Public

  /**
   * Use current coordinates to lookup address and populate form
   *
   */
  public async useCurrentAddress() {
    console.log('using current address!');
    this.coords = await this.ionicstorage.get('geolocation');
    console.log(this.coords);
    if (this.plt.is('cordova')) {
      if (this.showAddressForm) {
        this.showAddressForm = false;
      } else {
        this.location
          .getAddressFromCoordinates(this.coords[0], this.coords[1])
          .then((result: NativeGeocoderReverseResult) => {
            this.stylistRegForm.controls['addressLine1'].setValue(
              result.subThoroughfare
            );
            this.stylistRegForm.controls['addressLine2'].setValue(
              result.thoroughfare
            );
            this.stylistRegForm.controls['addressTownCity'].setValue(
              result.locality
            );
            this.stylistRegForm.controls['addressCounty'].setValue(
              result.subAdministrativeArea
            );
            this.stylistRegForm.controls['addressPostcode'].setValue(
              result.postalCode
            );

            this.stylistRegForm.controls['baseLocation'].setValue([
              this.coords[0],
              this.coords[1],
            ]);
            this.showAddressForm = true;
          })
          .catch((error: any) => console.error(error));
      }
    } else {
      // Do it for mobile web
    }
  }
  /**
   * Use address string to lookup coordinates and populate form.
   *
   */
  public findAddress() {
    console.log('finding address');
    console.log(this.stylistRegForm.get('baseLocation').value);
    if (this.plt.is('cordova')) {
      if (this.showAddressForm) {
        this.showAddressForm = false;
      } else {
        this.location
          .getCoordinatesFromAddress(
            this.stylistRegForm.get('locationLookup').value
          )
          .then((coords: NativeGeocoderForwardResult) => {
            this.location
              .getAddressFromCoordinates(coords.latitude, coords.longitude)
              .then((result: NativeGeocoderReverseResult) => {
                this.stylistRegForm.controls['addressLine1'].setValue(
                  result.subThoroughfare
                );
                this.stylistRegForm.controls['addressLine2'].setValue(
                  result.thoroughfare
                );
                this.stylistRegForm.controls['addressTownCity'].setValue(
                  result.locality
                );
                this.stylistRegForm.controls['addressCounty'].setValue(
                  result.subAdministrativeArea
                );
                this.stylistRegForm.controls['addressPostcode'].setValue(
                  result.postalCode
                );

                this.stylistRegForm.controls['baseLocation'].setValue([
                  coords.latitude,
                  coords.longitude,
                ]);

                this.showAddressForm = true;
              });
          })
          .catch((error: any) => console.error(error));
      }
    } else {
      // Do it for mobileweb
    }
  }

  public onSkillSelect(e) {
    console.log(e);

    const skillObj = e.map(s => {
      console.log(s);
      const skillContainer = { name } as any;

      skillContainer.name = s;

      return skillContainer;
    });

    console.log(skillObj);

    this.selectedSkills = skillObj;
  }
  /**
   * Submit stylist details
   *
   */
  public onSubmitStylistRegForm() {
    console.log(this.stylistRegForm.value);

    this.user
      .updateUserProfile(
        firebase.auth().currentUser.uid,
        this.stylistRegForm.value
      )
      .then(res => {
        console.log('Registered updated user - was stylist!', res);
        console.log('updated image refs');

        console.log(`update stylist skills ${this.stylistSkills}`);
        this.user.setStylistSkills(
          firebase.auth().currentUser.uid,
          this.stylistSkills
        );
        this.storage.setStorage('stylistRegistered', true);
        this.navCtrl.push('TabsPage', { isStylist: true });
        // });
      });
  }
  /**
   * Camera / Library action sheet
   *
   */
  public showImageActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: async () => {
            try {
              const res = await this.photo.getLibraryPictures();

              console.log(res);

              const baseRes = await this.photo.getBase64Data(
                res[0].photoFullPath,
                res[0].path
              );

              console.log(baseRes);

              const dummyUserId = '29BeWTmuNmZHwvMQoWDM2mnnKCS2';

              // spinner start
              const stored = await this.photo.pushPhotoToStorage(
                baseRes.photoFileName,
                baseRes.photoBase64Data,
                // replace with UID
                `${FirebaseStoragePaths.stylistGallery}/${dummyUserId}`
              );

              // spinner finish
              console.log(stored);

              this.profileUrl = stored;

              const userRes = await this.user
                .getUserById(dummyUserId)
                .valueChanges()
                .take(1)
                .toPromise();

              const existUser = userRes;

              const avatar = { avatarImage: this.profileUrl };

              const combined = { ...existUser, ...avatar };

              console.log(combined);

              const userPayload = {};

              userPayload[`userProfile/${dummyUserId}`] = combined;

              const dbRes = await this.afdb.database.ref().update(userPayload);
              console.log(dbRes);
            } catch (err) {
              console.error(err);
            }

            // const photos: any = res;
            // photos.forEach(el => {
            //   this.photo
            //     .getBase64Data(el.photoFullPath, el.path)
            //     .then(baseress => {
            //       console.log(baseress);
            // this.photo.pushPhotoToStorage(baseress).then(stores => {
            //   console.log(stores[0]);
            //   this.monitorUploadProgress(stores[0]);
            //       // });
            //     });
            // });
            // });
          },
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.photo
              .takePhoto(this.camera.PictureSourceType.CAMERA)
              .then(res => {
                console.log(res);
                // this.photo.getBase64Data(res).then(baseres => {
                // this.photo.pushPhotoToStorage(baseres).then(stores => {
                //   console.log(stores[0]);
                //   this.monitorUploadProgress(stores[0]);
                // });
                // });
              });
          },
        },
        {
          text: 'Cancel',
          role: 'cancel',
        },
      ],
    });
    actionSheet.present();
  }

  public goBack() {
    this.navCtrl.push('LoginPage');
  }

  // Slides

  public next() {
    this.slides.lockSwipeToNext(false);
    this.slides.slideNext();
    this.slides.lockSwipeToNext(true);
  }

  public back() {
    this.slides.lockSwipeToPrev(false);
    this.slides.slidePrev();
    this.slides.lockSwipeToPrev(true);
  }

  public onSlideChange(e) {
    console.log(this.slides.getActiveIndex());

    this.activeSlideIdx = this.slides.getActiveIndex();
  }
  /**
   *
   */
  public setIsMobile(value) {
    if (value) {
      this.outlineYes = false;
      this.outlineNo = true;
    } else {
      this.outlineYes = true;
      this.outlineNo = false;
    }

    this.stylistRegForm.controls['mobile'].setValue(value);
    if (!value) {
      setTimeout(() => {
        this.next();
      }, 1000);
    }
  }
  /**
   * Use camera to take photo and store in Firebase Storage and on userProfile/galleryImages
   * TODO Spinner or other UI to show something is going on
   */
  public async takePhoto() {
    const res = await this.photo.takePhoto(
      this.camera.PictureSourceType.CAMERA
    );
    console.log(res);
    let fullPath;
    if (this.plt.is('ios')) {
      fullPath = 'file://' + res;
    } else {
      fullPath = res;
    }

    let path = fullPath.substring(0, fullPath.lastIndexOf('/'));
    const base64 = await this.photo.getBase64Data(fullPath, path);

    const storage = await this.photo.pushPhotoToStorage(
      base64.photoFileName,
      base64.photoBase64Data
    );

    await this.addPhotosToGallery(storage.downloadURL);
  }
  /**
   * Select 1-n pictures from ImagePicker, store in Firebase Storage and on userProfile/galleryImages
   * TODO Spinner or other UI to show something is going on
   */
  public async selectPhoto(): Promise<any> {
    // This doesn't prevent the permissions dialog first time on imagepicker
    await this.imagePicker.requestReadPermission();

    const options = {
      width: 500,
      height: 500,
      quality: 100,
    };
    const images = await this.imagePicker.getPictures(options);

    const base64res = images.map(async (el, idx) => {
      let fullPath;

      if (this.plt.is('ios')) {
        fullPath = 'file://' + images[idx];
      } else {
        fullPath = images[idx];
      }

      const path = fullPath.substring(0, fullPath.lastIndexOf('/'));
      return await this.photo.getBase64Data(fullPath, path);
    });

    // As more than one photo can be selected, using Promise.all to resolve
    // when they have all completed base64 encoding
    // https://stackoverflow.com/questions/41673499/how-to-upload-multiple-files-to-firebase
    Promise.all(base64res).then(comp => {
      const storageRes = comp.map(async (el: any) => {
        console.log(el);
        return await this.photo.pushPhotoToStorage(
          el.photoFileName,
          el.photoBase64Data
        );
      });

      // Same here :-)
      Promise.all(storageRes).then(storecomp => {
        storecomp.map(async task => {
          return await this.addPhotosToGallery(task.downloadURL);
        });
      });
    });
  }

  // tslint:disable-next-line:cyclomatic-complexity
  public checkDisabled() {
    let required = false;

    switch (this.activeSlideIdx) {
      case 0:
        break;
      case 1:
        if (this.stylistRegForm.get('stylistName').errors) {
          required = this.stylistRegForm.get('stylistName').errors.required
            ? true
            : false;
        }
        break;
      case 2:
        if (this.stylistRegForm.get('bio').errors) {
          required = this.stylistRegForm.get('bio').errors.required
            ? true
            : false;
        }
        break;
      case 3:
        if (this.stylistRegForm.get('stylistSkills').errors) {
          required = this.stylistRegForm.get('stylistSkills').errors.required
            ? true
            : false;
        }
        break;
      case 4:
        if (this.stylistRegForm.get('phoneNumber').errors) {
          required = this.stylistRegForm.get('phoneNumber').errors
            ? true
            : false;
        }
        break;
      // case 4: // Location
      //   break;
      // case 5: // Mobile Stylist
      //   break;
      case 6: // Mobile Stylist
        if (this.stylistRegForm.get('mobileRange').errors) {
          required = this.stylistRegForm.get('mobileRange').errors
            ? true
            : false;
        }
        break;
      default:
    }

    return required;
  }

  // Private

  private monitorUploadProgress(tasks) {
    this.showPhotoSpinner = true;
    console.log('monitorUploadProgress');

    tasks.forEach(task => {
      console.log(task);
      task.on(
        'state_changed',
        (snapshot: any) => {
          this.loadProgress = (
            (snapshot.bytesTransferred / snapshot.totalBytes) *
            100
          ).toFixed(2);
          // this.loadProgress.push(prog);
          // console.log(this.loadProgress);
          console.log('Upload is ' + this.loadProgress + '% done');
          switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'
              console.log('Upload is paused');
              break;
            case firebase.storage.TaskState.RUNNING: // or 'running'
              console.log('Upload is running');
              break;
            default:
          }
          // return progress;
        },
        err => {
          console.error(err);
        },
        () => {
          console.log('success!');
          // Need the URLs for RTDB update
          // this.downloadUrls.push({ url: task.snapshot.downloadURL });
          this.downloadUrls.push(task.snapshot.downloadURL);
          this.showPhotoSpinner = false;
          console.log(this.downloadUrls);
        }
      );
    });
  }

  /**
   * Add file urls to array and call provider to add to Firebase Storage
   *
   */
  private addPhotosToGallery(url: string): Promise<void> {
    this.downloadUrls.push(url);

    return this.photo
      .addPhotosToUserGallery(this.downloadUrls)
      .then(res => console.log(res));
  }
}
