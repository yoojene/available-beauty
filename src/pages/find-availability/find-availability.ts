import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
} from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { AvailabilityProvider } from '../../providers/availability/availability';
import { UtilsProvider } from '../../providers/utils/utils';
import { Subject } from '../../../node_modules/rxjs/Subject';
import * as moment from 'moment';
import * as firebase from 'firebase';
import { LoginPage } from '../login/login';
import { BookingProvider } from '../../providers/booking/booking';
import { BookAvailabilityPage } from '../book-availability/book-availability';
import { Moment } from 'moment';
import { _ParseAST } from '@angular/compiler';
import { filter } from 'rxjs/operators';

@IonicPage()
@Component({
  selector: 'page-find-availability',
  templateUrl: 'find-availability.html',
})
export class FindAvailabilityPage {
  public findAvailabilityTitle = 'Select Availability Slot';
  public noAvailabilitiesText = 'No Availability';

  public stylistAvail$: Observable<any>; // define interface for Availbility
  public destroy$: Subject<any> = new Subject();
  public availabilities: any[] = [];
  public morning: any[] = [];
  public afternoon: any[] = [];
  public evening: any[] = [];
  public stylistUserId: any;

  private bookingId: string;
  public uid = firebase.auth().currentUser.uid;
  private anonymousUser = true;

  public viewDate: Moment = moment();
  public dates: any[] = [];
  public today = moment().format('Do MM YYYY');
  private date: any;
  public dayCounter: number = 0;
  public days: any[] = [];
  public day: any = {
    weekday: '',
  };

  private user: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public avail: AvailabilityProvider,
    public utils: UtilsProvider,
    private booking: BookingProvider,
    private modalCtrl: ModalController
  ) {
    firebase.auth().onAuthStateChanged(e => {
      this.anonymousUser = false;
      if (e.isAnonymous === true) {
        this.anonymousUser = true;
      }
    });
    this.user = this.navParams.get('user');

    this.buildCalendar();
    this.day = this.days[this.dayCounter];
    console.log(this.user);
  }

  public ionViewDidLoad() {
    console.log('ionViewDidLoad FindAvailabilityPage');
    this.getAvailability(this.user, this.day.date);
  }

  public onArrowTap(position) {
    if (position === 'left') {
      this.dayCounter--;
    } else {
      this.dayCounter++;
    }
    this.day = this.days[this.dayCounter];

    this.getAvailability(this.user, this.day.date);

    console.log(`day counter ${this.dayCounter}`);
    console.log(`day ${this.day.weekday}`);
  }

  public getAvailability(user: any, date: Moment) {
    console.log(user);
    console.log(date);
    this.stylistUserId = user.key;

    this.stylistAvail$ = this.avail
      .getStylistAvailability(user.key)
      .snapshotChanges();

    // this.stylist
    //   .getStylistReview(user.key)
    //   .valueChanges()
    //   .subscribe(res => (this.stylistReviews = res.length));

    this.stylistAvail$.takeUntil(this.destroy$).subscribe(actions => {
      const avails = this.utils.generateFirebaseKeyedValues(actions);

      this.availabilities = avails.filter(res => res.booked === false);

      this.availabilities.sort((a, b) => {
        return a.datetime - b.datetime;
      });

      this.availabilities = this.availabilities.filter(el => {
        return el.datetime >= moment().unix();
      });

      this.availabilities = this.availabilities.filter(el => {
        return moment
          .unix(el.datetime)
          .isBetween(moment(date).startOf('day'), moment(date).endOf('day'));
      });

      console.log('filtered availabilities');
      console.log(this.availabilities);

      this.availabilities.forEach(el => {
        // TODO group availabilities by day / month and display
        return (
          (el.origdatetime = el.datetime) &&
          (el.day = moment.unix(el.datetime).format('ddd Do')) &&
          (el.month = moment.unix(el.datetime).format('MMM')) &&
          (el.datetime = moment.unix(el.datetime).format('HH:mm')) // ddd Do MMM
        );
      });

      const format = 'HH:mm';
      const morningEndTime = moment('12:00', format);
      const afternoonEndTime = moment('15:30', format);
      this.morning = this.availabilities.filter(el => {
        return moment(el.datetime, format).isBefore(morningEndTime);
      });

      this.afternoon = this.availabilities.filter(el => {
        return moment(el.datetime, format).isBetween(
          morningEndTime,
          afternoonEndTime,
          null,
          '[]'
        );
      });

      this.evening = this.availabilities.filter(el => {
        return moment(el.datetime, format).isAfter(afternoonEndTime);
      });
    });
  }

  private async bookAvailability(avail) {
    if (this.anonymousUser) {
      return this.navCtrl.push(LoginPage, { isStylist: false });
    }
    // Make pending booking
    const result = await this.booking.makePendingBooking(
      avail.key,
      avail.origdatetime,
      avail.stylistId,
      this.uid
    );

    this.bookingId = result;

    const bookingModal = this.modalCtrl.create(BookAvailabilityPage, {
      availId: avail.key,
      stylist: avail.stylistId,
      userId: this.uid,
      bookId: this.bookingId,
    });

    bookingModal.onDidDismiss(data => {
      console.log('dismissed bookAvailabilityModal', data);
    });

    bookingModal.present();
  }

  private buildCalendar() {
    const interval = 1;
    let loopIdx = interval;
    for (let x = 0; x < 365; x++) {
      // console.log(loopIdx);
      this.dates.push({
        date: moment(this.viewDate).add(loopIdx, 'day'),
        weekday: moment(this.viewDate)
          .add(loopIdx, 'day')
          .format('DD-MMM-YYYY'),
        selected: false,
      });
      loopIdx += interval;
    }

    for (let x = 0; x < 7; x++) {
      this.days[x] = {
        date: moment(this.viewDate).add(x + 1, 'day'),
        weekday: moment(this.viewDate)
          .add(x + 1, 'day')
          .format('DD-MMM-YYYY'),
        selected: false,
      };
    }

    console.log(`days ${JSON.stringify(this.days)}`);
  }
  /**
   * Control selected button outline and return slots
   */
  public onDateTap(date, dates) {
    dates.forEach(el => {
      if (moment(el.date).isSame(moment(date.date))) {
        el.selected = !el.selected;
      } else if (el.selected) {
        el.selected = !el.selected;
      }
    });

    this.getAvailability(this.user, date.date);
  }

  public onAvailSelected(avail) {
    console.log(avail);

    this.bookAvailability(avail);
  }
  public onRadioTapped(avail) {
    console.log(avail);
  }
}
