import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController, ViewController } from 'ionic-angular';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { UserProvider } from '../../providers/user/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { StorageProvider } from '../../providers/storage/storage';
import { AngularFireDatabase } from '@angular/fire/database';

@IonicPage()
@Component({
  selector: 'page-buy-more-slots',
  templateUrl: 'buy-more-slots.html',
})
export class BuyMoreSlotsPage {
  private uid: string;
  private currentAvailableSlots: number;
  public products: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public iap: InAppPurchase,
    private platform: Platform,
    private toastCtrl: ToastController,
    private viewCtrl: ViewController,
    private af: AngularFireAuth,
    private storage: StorageProvider,
    private user: UserProvider,
    private afdb: AngularFireDatabase
  ) {
    
    this.storage
    .getStorage('current_available_slots')
    .subscribe(result => (this.currentAvailableSlots = parseInt(result)));

    this.afdb.database.ref('/settings').on('value', (snapshot) => {
      let settings = snapshot.val();
      let isIos = this.platform.is('ios');

      this.products = [
        {
          name: settings.smallProductAlias,
          price: isIos ? settings.smallProductPriceIos: settings.smallProductPrice,
          slots: settings.smallProductSlots,
          image: "assets/img/card-amsterdam.png"
        },
        {
          name: settings.mediumProductAlias,
          price: isIos ? settings.mediumProductPriceIos: settings.mediumProductPrice,
          slots: settings.mediumProductSlots,
          image: "assets/img/card-sf.png"
        },
        {
          name: settings.largeProductAlias,
          price: isIos ? settings.largeProductPriceIos: settings.largeProductPrice,
          slots: settings.largeProductSlots,
          image: "assets/img/card-saopaolo.png"
        }
      ];
      
      let productIds = [
        'com.availablebeauty.10_slots',
        'com.availablebeauty.20_slots',
        'com.availablebeauty.50_slots'
      ];
  
      this.platform.ready().then(() => {
        this.iap.getProducts(productIds)
        .then((products) => {
          console.log(`Success iap get products ${JSON.stringify(products)}`);
        })
        .catch((error) => {
          console.log(`Error iap get products ${error}`);
        })
      });
  
      const currentUser = this.af.auth.currentUser;
      this.uid = currentUser.uid;

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuyMoreSlotsPage');
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter BuyMoreSlotsPage');
  }

  public buyProduct(packageId) {
    let packages = [
      'com.availablebeauty.10_slots',
      'com.availablebeauty.20_slots',
      'com.availablebeauty.50_slots'
    ];

    let item = packages[packageId - 1];
    this.iap.buy(item)
      .then((data) => {
        console.log(`Buy success ${JSON.stringify(data)}`);

        try {
          if (data.transactionId) {
            console.log(data.transactionId);
            this.currentAvailableSlots = this.currentAvailableSlots + this.products[packageId - 1].slots;
            this.storage.setStorage('current_available_slots', this.currentAvailableSlots);
  
            this.user.setStylistAvailableSlots(this.uid, this.currentAvailableSlots);
          }
        } catch (error) {
          console.log(`Error ${error}`);
        }
        
        let toast = this.toastCtrl.create({
          message: 'Thank you for purchasing!',
          duration: 3000,
          position: 'top',
          dismissOnPageChange: true,
          showCloseButton: true,
        });
        toast.present();

        toast.onDidDismiss((data, role) => {
          this.viewCtrl.dismiss().then();
        });

        return this.iap.consume(data.productType, data.receipt, data.signature);
      })
      .then((data) => {
        console.log(`Product successfully consumed ${JSON.stringify(data)}`);
      })
      .catch((error) => {
        let errorToast = this.toastCtrl.create({
          message: 'Failed to purchase slots. Please try again later. If problem persists please contact developer.',
          duration: 3000,
          position: 'top',
          showCloseButton: true
        });
        errorToast.present();
        console.log(`Buy error ${JSON.stringify(error)}`);
      });
  }

}
