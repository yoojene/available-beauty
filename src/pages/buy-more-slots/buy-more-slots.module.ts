import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuyMoreSlotsPage } from './buy-more-slots';

@NgModule({
  declarations: [
    BuyMoreSlotsPage,
  ],
  imports: [
    IonicPageModule.forChild(BuyMoreSlotsPage),
  ],
})
export class BuyMoreSlotsPageModule {}
