import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LocationProvider } from '../providers/location/location';

import { AngularFireAuth } from '@angular/fire/auth';
import { UserProvider } from '../providers/user/user';

@Component({
  templateUrl: 'app.html',
})
export class AvailableBeautyApp {
  @ViewChild('#myNav')
  public ubnav: NavController;
  public rootPage: string = 'LandingPage';
  public stylistParam: any;

  public lat: number;
  public long: number;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private location: LocationProvider,
    private afAuth: AngularFireAuth,
    private user: UserProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();

      // Watch GeoLocation in all platforms (including browser)
      this.watchGeoLocation();

      // this.checkAuthState();
    });
  }

  private watchGeoLocation() {
    this.location.watchGeoLocation();
  }

  private checkAuthState() {
    console.log('checking auth state....');
    this.afAuth.authState.subscribe(auth => {
      if (!auth) {
        // Unauthenticated state
        this.rootPage = 'LandingPage';
      } else {
        const uid = auth.uid;

        // Check if is Stylist or User
        this.user.checkIsStylist(uid).subscribe(res => {
          if (!res) {
            this.rootPage = 'TabsPage';
          } else {
            this.user
              .getUserById(uid)
              .valueChanges()
              .subscribe(user => {
                console.log(user);
                if (!user.hasOwnProperty('stylistProfileComplete')) {
                  // Temporarily reversed this check
                  // console.log('profile setup');
                  // this.rootPage = 'TabsPage';
                  this.rootPage = 'AvailabilityPage';
                } else {
                  this.rootPage = 'StylistRegisterPage';
                }
              });
          }
        });
      }
    });
  }
}
