import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpandableItemComponent } from './expandable-item/expandable-item';
import { ProgressBarComponent } from './progress-bar/progress-bar';
import { StarRatingComponent } from './star-rating/star-rating';
// import { FavouriteIndicatorComponent } from './favourite-indicator/favourite-indicator';
import { ChatBubbleComponent } from './chat-bubble/chat-bubble';
import { StylistMapsComponent } from './stylist-maps/stylist-maps';

@NgModule({
  declarations: [
    ExpandableItemComponent,
    ProgressBarComponent,
    StarRatingComponent,
    // FavouriteIndicatorComponent,
    ChatBubbleComponent,
    StylistMapsComponent,
  ],
  imports: [CommonModule],
  exports: [
    ExpandableItemComponent,
    ProgressBarComponent,
    StarRatingComponent,
    // FavouriteIndicatorComponent,
    ChatBubbleComponent,
    StylistMapsComponent,
  ],
  entryComponents: [StarRatingComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ComponentsModule {}
