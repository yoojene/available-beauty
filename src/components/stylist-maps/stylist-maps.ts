import { Component, ViewChild, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { StorageProvider } from '../../providers/storage/storage';

declare const google;

@Component({
  selector: 'stylist-maps',
  templateUrl: 'stylist-maps.html',
})
export class StylistMapsComponent implements OnChanges {
  @Input() public lat;
  @Input() public long;

  @Input() public stylists: any;

  @ViewChild('map') public mapElement: ElementRef;
  public map: any;

  constructor(public storage: StorageProvider) {
    console.log('Hello StylistMapsComponent Component');
  }

  // Lifecycle
  public ngOnInit() {
    this.getGeoLocation();
  }

  public ngOnChanges(changes: SimpleChanges) {
    console.log(changes);

    if (changes.stylists) {
      this.stylists = changes.stylists.currentValue;
      // this.addStylistMarker();
    }
  }

  /**
   * Create new Google map centred on current device location
   */
  private loadMap() {
    console.log('JS map loading');
    const latLng = new google.maps.LatLng(this.lat, this.long);

    const mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    this.addStylistMarker();
  }

  /**
   *Add map markers for Stylists in search results
   *
   */
  private addStylistMarker() {
    // const testMarker = new google.maps.Marker({
    //   map: this.map,
    //   animation: google.maps.Animation.DROP,
    //   position: this.map.getCenter()
    // });

    // console.log(testMarker);

    console.log(this.stylists);

    this.stylists.forEach(sty => {
      const marker = new google.maps.Marker({
        position: { lat: sty.baseLocation[0], lng: sty.baseLocation[1] },
        map: this.map,
        title: sty.stylistName,
      });

      const content = `<h4>${sty.stylistName}</h4>
                       <p>${sty.bio}</p>`;

      this.addInfoWindow(marker, content);

      console.log(marker);
    });
  }
  /**
   * Build map marker info window and click event
   *
   */
  private addInfoWindow(marker, content) {
    const infoWindow = new google.maps.InfoWindow({
      content,
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }
  /**
   * Return geolocation from storage and build map
   */
  private getGeoLocation() {
      this.storage.getStorage('geolocation').subscribe(res => {
        if (res) {
          this.lat = res[0];
          this.long = res[1];
          console.log(this.lat);
          console.log(this.long);

          this.loadMap();
        }
      });
    }
}
